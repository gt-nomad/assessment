## About This
Assestment DevOps Test

## Question 1:
* Create Dockerfile for java spring boot image

## Question 2:
* Design your ideal (without any budget and infrastructure constraint) end to end
  CI/CD Flow for Mobile and Backend using kubernetes as your cloud platform.

## Question 3:
* Create Jenkinsfile based on CI/CD flow that you design on the Question number 2.

## Question 4:
* Create deployment.yaml, service.yaml and ingress.yaml based on number 1

## local test docker springboot hello-world
$ docker build . -t hello-word/springboot:local
$ docker run -d --name hello-world -p 127.0.0.1:80:8080 hello-world/springboot:local

## NOTED:
* Step 1 Install jenkins and regristry using docker-compose
  $ docker-compose up -d
* Step 2 start jenkins using image liatrio/jenkins-alpine (this is support pipeline docker)
  $ docker run -p 8080:8080 -v /var/run/docker.sock:/var/run/docker.sock liatrio/jenkins-alpine
* Step 3 Install minikube
  $ curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube_latest_amd64.deb
  $ sudo dpkg -i minikube_latest_amd64.deb
  $ minikube start
